﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Handler.Entities;



namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    class CustomerContext : DbContext
    {

        public DbSet<Customer> Customers { get; set; }

        public CustomerContext()
        {
            Database.EnsureCreated();
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
               {
                   optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=hwlesson23-3;Username=postgres;Password=12345");
              }
    }
}
