﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {


        public void AddCustomer(Customer customer)
        {

            using (CustomerContext db = new CustomerContext())
            {


                db.Customers.Add(customer);
                db.SaveChanges();

            }

        }
    }
} 

