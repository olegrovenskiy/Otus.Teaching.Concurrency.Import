﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Core.Repositories;



namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
       // string FileDir = "C:\\Users\\o.rovenskiy\\source\\repos\\Otus.Teaching.Concurrency.Import\\ForFiles\\";
       // string _dataFileName = "NewTestFileOleg.xml";
       // string _processFileName = "ProcessNewTestFileOleg";

        private static string _dataFilePath = "C:\\Users\\o.rovenskiy\\source\\repos\\Otus.Teaching.Concurrency.Import\\ForFiles\\" + "NewTestFileOleg";

       // private static string _dataFilePathMethod = "C:\\Users\\o.rovenskiy\\source\\repos\\Otus.Teaching.Concurrency.Import\\ForFiles\\" + "MethodNewTestFileOleg";

        private static string ProcessFileExeName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private static string ProcessFileDirName = "C:\\Users\\o.rovenskiy\\source\\repos\\Otus.Teaching.Concurrency.Import\\Otus.Teaching.Concurrency.Import.DataGenerator.App\\bin\\Debug\\netcoreapp3.1\\";

        //private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");
        
        static void Main(string[] args)
        {
            
            
            
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");



            Console.WriteLine("Если вы хотите запустимть процесс генерации файла, скажите yes");
            var yes = Console.ReadLine();

            if (yes != "yes")

            {
                GenerateCustomersDataFile();
            }

            else
            {
                using Process process = new Process();
                {
                    process.StartInfo.FileName = ProcessFileDirName + ProcessFileExeName; //путь к приложению, которое будем запускать
                    process.StartInfo.WorkingDirectory = ProcessFileDirName; //путь к рабочей директории приложения
                    process.StartInfo.Arguments = _dataFilePath; //аргументы командной строки (параметры)
                    process.Start();
                };

            }

            var aaa = new CustomerRepository();

            var loader = new DataLoader(aaa);

            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath + ".xml", 1000);
            xmlGenerator.Generate();
        }
    }
} 