using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader : IDataLoader
    {
        string _dataFileName = "NewTestFileOleg.xml";
        string FileDir = "C:\\Users\\o.rovenskiy\\source\\repos\\Otus.Teaching.Concurrency.Import\\ForFiles\\";
        
        
        private readonly ICustomerRepository _customerRepository;
        public DataLoader (ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }


        // ������� �������� ������ � ��������� �� ������
        public void LoadData()
        {

            string FilePath = FileDir + _dataFileName;
            var sss = new DataParser<CustomersList>(FilePath);
            var ddd = sss.Parse();
            int threadsNumber = 4; // ���-�� �������, 4 ��� �������

            int newQuantiti = ddd.Customers.Count / threadsNumber; // ���-�� �������� � ������

            
            List<List<Customer>> ListOfList = new List<List<Customer>>(); // ������ ������� � ��������� ����� �������� ��� �������

            // ���������� �� ������ ��� �������

            for (int j = 0; j < threadsNumber; j++)
            {
                List<Customer> Splited = new List<Customer>();
                int delta = 0;

                if (j == threadsNumber-1)
                {
                    delta = threadsNumber * newQuantiti - ddd.Customers.Count; // ��� ������ ����� ��������� ������ ����� ���������� �� ������
                }

                for (int h = 0+j*newQuantiti; h < newQuantiti + j*newQuantiti + delta; h++)

                {
                    Splited.Add(ddd.Customers[h]);

                }

                ListOfList.Add(Splited);
               
            }

            // ��������� ������� ����� 



            Console.WriteLine(" �����         ");


}



            Task tasks1 = new Task(() => LoaderOfCustomers((ListOfList[0])));
            Task tasks2 = new Task(() => LoaderOfCustomers((ListOfList[1])));
            Task tasks3 = new Task(() => LoaderOfCustomers((ListOfList[2])));
            Task tasks4 = new Task(() => LoaderOfCustomers((ListOfList[3])));



            tasks1.Start();
            tasks2.Start();
            tasks3.Start();
            tasks4.Start();


            tasks1.Wait();
            tasks2.Wait();
            tasks3.Wait();
            tasks4.Wait();

           

        public void LoaderOfCustomers (List<Customer> customersList) 
        {

            foreach ( var customer in customersList)
            {
                Console.WriteLine(customer.FullName);
                               
               _customerRepository.AddCustomer(customer);

            }



        }




}
}