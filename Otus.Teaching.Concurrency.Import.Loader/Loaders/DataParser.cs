﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataParser<T> : IDataParser<T> where T : class
    {
        string FilePath { get; set; }
        T Value { get; set; }

        public DataParser(string filePath)
        {
            FilePath = filePath;

        }

        public T Parse()
        {
            //throw new NotImplementedException();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate))
            {

              // T Customers1 = new ValueTuple();


                return xmlSerializer.Deserialize(fs) as T;



            }



        }
    }
}

